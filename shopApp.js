var app = new Vue({
  el: '#app',
  data: {
    selected2: '',
    products: [],
    items: [],
  },
  mounted() {
    axios.get('http://localhost:3000/productList')
      .then(response => {
        this.products = response.data;
        console.log(this.products);
      })
      .catch(function (error) {
        console.log(error);
      });
  },
  computed: {
  total() {
    var total = 0;
    for(var i = 0; i < this.items.length; i++) {
      total += this.items[i].price;
    }
    return total;
  }
},
  methods: {
    addToCart(model) {
      model.quantity += 1;
      this.items.push(model);
    },
  }
});
